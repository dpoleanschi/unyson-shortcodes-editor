<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'general-tab' => array(
		'title'   => '',
		'type'    => 'box',
		'options' => array(
			'post_types' => array(
				'label'   => __( 'Activate for', 'fw' ),
				'type'    => 'checkboxes',
				'choices' => fw_ext_dummy_shortcodes_get_supported_post_types(),
				'value'   => array(
					'page' => true
				),
				'desc'    => __( 'Select the options you want the Dummy Shortcodes extension to be activated for', 'fw' )
			),
			apply_filters('fw_ext_dummy_shortcodes_options', array())
		)
	)
);